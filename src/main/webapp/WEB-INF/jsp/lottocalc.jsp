<!DOCTYPE html>
<html>
<head>
<title>Welcome to Dallas Citgo</title>
<style>
html {
margin : 80px;

}
body {
font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
}
table, th, td {
  border: 1px solid black;
  text-align: center;
}
input {
  border-style: hidden;
}
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}
	
#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  background-color: #8a1212;
  color: white;
}
</style>
</head>
<body>

	<center><h1>Scratch-off sales calculator</h1></center> 
	<br>Date : ${date} <br>

	<form method="POST">
		<table id="customers">
			<tr>
				<th>S.No</th>
				<th>Amount</th>
				<th>Shift open</th>
				<th>Shift close</th>
				<th> Total </th>
			
			</tr>
			<tr>
				<td>1</td>
				<td><input type="number" name="am1" value=1></td>
				<td><input type="text" name="a1" required></td>
				<td> <input type="text" name="a2" required></td>
				<td>${am1}</td>
			
			</tr>
			
			<tr>
				<td>2</td>
				<td><input type="text" name="am2" value=5></td>
				<td><input type="text" name="b1" required></td>
				<td> <input type="text" name="b2" required></td>
				<td><input type="text" name="b3" required></td>
			</tr>
			
			<tr>
				<td>3</td>
				<td><input type="text" name="am3" value=10></td>
				<td><input type="text" name="c1" ></td>
				<td> <input type="text" name="c2" ></td>
				<td><input type="text" name="c3" ></td>
			</tr>
			
			<tr>
				<td>4</td>
				<td><input type="text" name="am4" value=30></td>
				<td><input type="text" name="d1" ></td>
				<td> <input type="text" name="d2" ></td>
				<td><input type="text" name="d3" ></td>
			</tr>
			
			<tr>
				<td>4</td>
				<td><input type="text" name="am5" value=25></td>
				<td><input type="text" name="e1" ></td>
				<td> <input type="text" name="e2" ></td>
				<td><input type="text" name="e3" ></td>
			</tr>
				
		</table>
		<br><br>
		<input type="submit" name="submit" />

	</form>
	
	<h3>${result}  ${fintotal} </h3>

</body>
</html>