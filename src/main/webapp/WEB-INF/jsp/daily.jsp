<!DOCTYPE html>
<html>
<head>
<title>Welcome to Dallas Citgo</title>
<style>
html {
margin : 80px;

}
body {
font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
}
table, th, td {
  border: 1px solid black;
  text-align: center;
}
input {
  border-style: hidden;
}
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}
	
#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  background-color: #8a1212;
  color: white;
}
</style>
</head>
<body>

	<center><h1>Daily paperwork</h1></center> 
	<br>Date : ${date} <br>

	<form method="POST">
		<table id="customers">
			<tr>
				<th>Detail</th>
				<th>Amount</th>
				<th>Detail</th>
				<th>Amount</th>
			</tr>
			<tr>
				<td>Cash</td>
				<td><input type="text" name="cash" required> ${cash}</td>
				<td>Fuel</td>
				<td><input type="text" name="fuel" required>${fuel}</td>
			</tr>
			<tr>
				<td>Credit</td>
				<td><input type="text" name="credit" required>${credit}</td>
				<td>Hi-Grocery</td>
				<td><input type="text" name="hgrocery" required>${hgrocery}</td>
			</tr>
			<tr>
				<td>Debit</td>
				<td><input type="text" name="debit" required>${debit}</td>
				<td>Lo-Grocery</td>
				<td><input type="text" name="lgrocery" required>${lgrocery}</td>
			</tr>
			<tr>
				<td>Instant lotto cash</td>
				<td><input type="text" name="instantlotto" required> ${instantlotto}</td>
				<td>Lottery Scratch sales</td>
				<td><input type="text" name="scratchsale" required>${scratchsale}</td>
			</tr>
			<tr>
				<td>Online lotto cash</td>
				<td><input type="text" name="onlinelotto" required> ${onlinelotto}</td>
				<td>Lottery Online sales</td>
				<td><input type="text" name="onlinesale" required>${onlinesale}</td>
			</tr>
			<tr>
				<td>Vendor Payout</td>
				<td><input type="text" name="vendor" required> ${vendor}</td>
				<td>Cigarette</td>
				<td><input type="text" name="cig" required>${cig}</td>
			</tr>
			<tr>
				<td>Credit Card</td>
				<td><input type="text" name="creditcard" required> ${creditcard}</td>
				<td>Puffs & bidi</td>
				<td><input type="text" name="puffs" required>${puffs}</td>
			</tr>
			<tr>
				<td>Others</td>
				<td><input type="text" name="others" required> ${others}</td>
				<td>Cigar</td>
				<td><input type="text" name="cigar" required>${cigar}</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Misc</td>
				<td><input type="text" name="misc" required>${misc}</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Misc</td>
				<td><input type="text" name="tax" required>${tax}</td>
			</tr>
			<tr>
				<td> Total </td>
				<td>${total1}</td>
				<td>Total : </td>
				<td>${total2}</td>
			</tr>
				
		</table>
		<br><br>
		<input type="submit" name="submit" />

	</form>
	
	<h3>${result}  ${fintotal} </h3>

</body>
</html>