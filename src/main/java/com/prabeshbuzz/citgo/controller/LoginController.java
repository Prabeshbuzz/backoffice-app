package com.prabeshbuzz.citgo.controller;

import java.time.LocalDate;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {
	
	@RequestMapping("/login")
	public String Login(ModelMap model) {
		
		return "login";
	}
	
	@RequestMapping("/daily")
	public String showdaily(ModelMap model) {
		//model.put("name", name);
		return "daily";
	}
	
	
	@PostMapping("/daily")
	public String daily(@RequestParam double cash,@RequestParam double fuel,@RequestParam double hgrocery,@RequestParam double lgrocery,@RequestParam double scratchsale,
			@RequestParam double onlinesale,@RequestParam double cig,@RequestParam double creditcard,@RequestParam double puffs,@RequestParam double credit,
			@RequestParam double debit,@RequestParam double instantlotto,@RequestParam double cigar,@RequestParam double tax,
			@RequestParam double onlinelotto,@RequestParam double vendor,@RequestParam double others,@RequestParam double misc,ModelMap model,Double total1,Double total2) 
	
	{
		//model.put("name", name);
		model.put("cash", cash);
		model.put("credit", credit);
		model.put("debit", debit);
		model.put("instantlotto", instantlotto);
		model.put("onlinelotto", onlinelotto);
		model.put("vendor", vendor);
		model.put("others", others);
		model.put("creditcard", creditcard);
		model.put("fuel", fuel);
		model.put("hgrocery", hgrocery);
		model.put("lgrocery", lgrocery);
		model.put("onlinesale", onlinesale);
		model.put("scratchsale", scratchsale);
		model.put("cig", cig);
		model.put("puffs", puffs);
		model.put("misc", misc);
		model.put("puffs", cigar);
		model.put("tax", tax);
		
		total1 = (double) (cash + credit + debit + instantlotto + onlinelotto + vendor + others + creditcard );
		total2= ( double) (fuel + hgrocery + lgrocery + onlinesale + scratchsale + cig + puffs + misc + cigar +tax);
		model.put("total1",total1);
		model.put("total2",total2);
		LocalDate localDate = LocalDate.now();
		String date = localDate.toString();
		model.put("date",date);
		String output;
		if (total1>total2) {
			double fintotal = total1 - total2;
			String result = "Over is :";
			model.put("result", result);
			model.put("fintotal",fintotal);
		}
		else {
			double fintotal = total2 - total1;
			model.put("fintotal", fintotal);
			String result = "Short is :";
			model.put("result", result);
		}
		return "daily";
		
		
	}
	
	@GetMapping("/lottocalc")
	public String showlotto(ModelMap model) {
		

		
	
	
		
		return "lottocalc";
		
	}
	
	@PostMapping("/lottocalc")
	public String lotto(@RequestParam double a1,@RequestParam double a2,
			@RequestParam double b1,@RequestParam double b2,
			@RequestParam double c1,@RequestParam double c2,ModelMap model) {
		
		model.put("a1",a1);
		model.put("a2",a2);
		
		double am1 = a1 + a2;
		model.put("am1",am1);
		
		

	
		
		return "lottocalc";
		
	}
}


