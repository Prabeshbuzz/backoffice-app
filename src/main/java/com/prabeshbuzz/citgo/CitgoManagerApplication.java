package com.prabeshbuzz.citgo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CitgoManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CitgoManagerApplication.class, args);
	}

}
